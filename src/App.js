import logo from './logo.svg';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import {useState, Fragmnet, useEffect} from 'react';
const axios = require("axios").default;

function App() {
  const [temp, setTemp]=useState([])
  const apikey = '01ef33c018898695016f103781615ab2'
  useEffect(() => {
    const getData = () =>{
      var options = {
        method: 'GET',
        url: `https://api.openweathermap.org/data/2.5/weather?q=London,uk&appid=${apikey}&units=metric`,
      };

      axios.request(options).then(function (response) {
        setTemp(response.data.main)
        console.log(response.data);
      }).catch(function (error) {
        console.error(error);
      });
    }
    getData()
  },[])
  return (
    <div className="App">
      <h1>Weather App</h1>
      <div className="container">
        <div className="row">
          <div className="col-lg-6 offset-lg-3">
          <br/>
          <br/>
          <br/>
            <h2>london, UK</h2>
            <h2>
            Temp:
            {temp.temp}°C
            </h2>
            <h2>
            Max Temp:
            {temp.temp_max}°C
            </h2>
            <h2>
            Feels Like:
            {temp.feels_like}°C
            </h2>
            <h2>
            Humidity:
            {temp.humidity}%
            </h2>
            <h2>
            Pressure:
            {temp.pressure} Pa
            </h2>
          </div>
        </div>

      </div>
    </div>
  );
}

export default App;
